import 'package:flutter/material.dart';

void main() {
  runApp(ClassTwo());
}

class ClassTwo extends StatelessWidget {
  const ClassTwo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
      backgroundColor: Colors.grey.shade400,
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: Image.asset("assets/images/ProfilePic.jpg", height: 420),
              ),
              const SizedBox(
                height: 5,
              ),
              const Text(
                "John Warner",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w700,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              const Text(
                "Software Engineer",
                style: TextStyle(
                  fontFamily: "Satisfy",
                  fontSize: 24,
                  fontWeight: FontWeight.w700,
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset("assets/icons/facebooklogo.png"),
                  const SizedBox(
                    width: 10,
                  ),
                  Image.asset("assets/icons/apple.png"),
                  const SizedBox(
                    width: 10,
                  ),
                  Image.asset("assets/icons/google.png"),
                ],
              ),
              const SizedBox(
                height: 8,
              ),
              Container(
                padding: const EdgeInsets.only(left: 25.0),
                child: const Text(
                  "I am highly positive thinker,good team leader, can work under pressure",
                  style: TextStyle(
                    letterSpacing: .1,wordSpacing: 0,
                    fontSize: 18,
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(left: 50,top: 8),
                child: ListTile(
                  visualDensity: VisualDensity(horizontal: -4, vertical: -4),
                  leading: Icon(
                    Icons.phone,
                    color: Colors.black,
                  ),
                  title: Text(
                    "+88 0123 1182348",
                    style: TextStyle(fontSize: 15, color: Colors.black),
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(left: 50),
                child: ListTile(
                  visualDensity: VisualDensity(horizontal: -4, vertical: -4),
                  contentPadding: EdgeInsets.only(top:0 ,left: 15),
                  leading: Icon(
                    Icons.email,
                    color: Colors.black,
                  ),
                  title: Text(
                    "Mymail01@gmail.com",
                    style: TextStyle(fontSize: 15, color: Colors.black),
                  ),
                ),
              ),
              ElevatedButton.icon(
                onPressed: () {},
                style: ElevatedButton.styleFrom(primary: Colors.black),
                label: const Text('Hire Me Now'),
                icon: const Icon(Icons.add_task_outlined),
              )
            ],
          ),
        ),
      ),
    );
  }
}
